# Summary
T-12 is a 2.5D rapid pace, top-down, infinite shooter game. The gameplay is simple; the player will control a plane and shoot down enemy targets while evading shots in return. The player will be given a score, lives, health, with the goal of accumulating as many points as possible.
(Note that this is a basic prototype and not the final version).

## Platform
PC, Mac, Linux

## Download
[T-12.zip] (https://goo.gl/8QjUsp "T-12 Game and Data")
* Unzip the downloadable and run T-12.exe.
* The T-12.exe and T-12_Data must be together to execute.

## Note
* __T-12 is a basic prototype but the continuing development can be found here...__
* https://gitlab.com/jimmyh3/JH3-AerialCombat